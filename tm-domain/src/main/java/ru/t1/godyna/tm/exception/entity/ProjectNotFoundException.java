package ru.t1.godyna.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
