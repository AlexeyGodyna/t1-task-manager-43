package ru.t1.godyna.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public final class User extends AbstractModel {

    @Nullable
    @Column(name = "login", nullable = true, length = 50)
    private String login;

    @Nullable
    @Column(name = "password_hash", nullable = true, length = 200)
    private String passwordHash;

    @Nullable
    @Column(name = "email", nullable = true)
    private String email;

    @Nullable
    @Column(name = "fst_name", nullable = true, length = 50)
    private String firstName;

    @Nullable
    @Column(name = "lst_name", nullable = true, length = 50)
    private String lastName;

    @Nullable
    @Column(name = "mdl_name", nullable = true, length = 50)
    private String middleName;

    @NotNull
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "lock_flg", nullable = false)
    private boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

    public boolean isLocked() {
        return locked;
    }

}
